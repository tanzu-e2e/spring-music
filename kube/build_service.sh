#!/bin/bash -x

. kube/kubeconfig.sh $build_cluster_context

kp image status "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH" > /dev/null 2>&1 || image=0

REGISTRY_IMAGE="${EXT_REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}"
                  
echo $REGISTRY_IMAGE

if [[ $image != 0 ]]; then
  kp image patch "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH" \
  --git "${CI_PROJECT_URL}.git" \
  --git-revision "$CI_COMMIT_SHA" \
  -w
else
  kp image create "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH" \
  --tag "$REGISTRY_IMAGE/$CI_COMMIT_BRANCH" \
  --git "${CI_PROJECT_URL}.git" \
  --git-revision "$CI_COMMIT_SHA" \
  --env SPRING_CONFIG_LOCATION="/app-config/" \
  --cluster-builder default -w
fi
