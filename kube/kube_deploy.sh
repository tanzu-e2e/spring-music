#!/bin/bash -x


#if env is vsp set domain accordingly, default to normal ingress domain otherwise
if [ $env = 'vsp' ]
then
  ingress_domain=$vsp_insgress_domain
  cluster_context_spec='vsphere7'
fi

. kube/kubeconfig.sh $cluster_context_spec

REGISTRY_IMAGE="${EXT_REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}"


ytt -f kube/deployment.yaml -f values.yaml -v root_dns_name=${ingress_domain} -v registry=${REGISTRY_IMAGE} -v namespace=$namespace -v image_name=${CI_COMMIT_BRANCH} -v tag=latest -v app_name=$CI_PROJECT_NAME |  kubectl --kubeconfig=$kube_config -n $namespace apply -f-

kubectl rollout restart deployment.apps/$CI_PROJECT_NAME -n $namespace
